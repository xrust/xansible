use expectrl::{
    interact::{Context, InteractOptions},
    Session,
    WaitStatus::Exited,
};
use promptly::prompt_opt;
use serde::ser::Serialize;
use std::borrow::Cow;
use std::path::{Path, PathBuf};
use std::process::{exit, Command};

#[derive(Clone, Debug)]
pub struct Ansible {
    inventory_dir: String,
    vault_pass: String,
    tags: Vec<String>,
    envs: Vec<(String, String)>,
    extra_vars: Vec<String>,
    hidden_extra_vars: Vec<String>,
    prompts: Vec<(String, String)>,
    pass_prompts: Vec<String>,
    verbosity: usize,
    cwd: Option<PathBuf>,
    check: bool,
    diff: bool,
}

impl Ansible {
    pub fn new(inventory_dir: String, vault_pass: String) -> Self {
        Self {
            inventory_dir,
            vault_pass,
            envs: vec![],
            tags: vec![],
            extra_vars: vec![],
            hidden_extra_vars: vec![],
            verbosity: 0,
            prompts: vec![],
            pass_prompts: vec!["Vault password:".into()],
            cwd: None,
            check: false,
            diff: false,
        }
    }

    pub fn check(&self, enable: bool) -> Self {
        let mut self2 = (*self).clone();
        self2.check = enable;
        self2
    }

    pub fn diff(&self, enable: bool) -> Self {
        let mut self2 = (*self).clone();
        self2.diff = enable;
        self2
    }

    pub fn set_cwd(&self, cwd: &Path) -> Self {
        let mut self2 = (*self).clone();
        self2.cwd = Some(cwd.to_owned());
        self2
    }

    pub fn hidden_extra_var(&self, k: impl ToString, v: impl ToString) -> Self {
        let mut self2 = (*self).clone();
        let k = k.to_string();
        let v = v.to_string();
        self2.hidden_extra_vars.push(format!("{k}={v}"));
        self2
    }

    pub fn hidden_extra_vars(&self, obj: impl Serialize) -> Self {
        let mut self2 = (*self).clone();
        self2
            .hidden_extra_vars
            .push(serde_json::to_string(&obj).unwrap());
        self2
    }

    pub fn env(&self, k: impl ToString, v: impl ToString) -> Self {
        let mut self2 = (*self).clone();
        self2.envs.push((k.to_string(), v.to_string()));
        self2
    }

    pub fn extra_var(&self, k: impl ToString, v: impl ToString) -> Self {
        let mut self2 = (*self).clone();
        let k = k.to_string();
        let v = v.to_string();
        self2.extra_vars.push(format!("{k}={v}"));
        self2
    }

    pub fn extra_vars(&self, obj: impl Serialize) -> Self {
        let mut self2 = (*self).clone();
        self2.extra_vars.push(serde_json::to_string(&obj).unwrap());
        self2
    }

    pub fn tag(&self, tag: impl ToString) -> Self {
        let mut self2 = (*self).clone();
        self2.tags.push(tag.to_string());
        self2
    }

    pub fn expect_prompt(&self, prompt: impl ToString, resp: impl ToString) -> Self {
        let mut self2 = (*self).clone();
        self2.prompts.push((prompt.to_string(), resp.to_string()));
        self2
    }

    pub fn pass_prompt(&self, prompt: impl ToString) -> Self {
        let mut self2 = (*self).clone();
        self2.pass_prompts.push(prompt.to_string());
        self2
    }

    #[allow(dead_code)]
    pub fn verbose(&self) -> Self {
        let mut self2 = (*self).clone();
        self2.verbosity = 2;
        self2
    }

    #[allow(dead_code)]
    pub fn verbosity(&self, level: usize) -> Self {
        let mut self2 = (*self).clone();
        self2.verbosity = level;
        self2
    }

    pub fn play(&self, playbook: impl AsRef<str>) {
        let mut cmd = Command::new("ansible-playbook");
        cmd.envs(self.envs.clone());
        if let Some(cwd) = &self.cwd {
            cmd.current_dir(cwd);
        }
        cmd.args(["-i", &self.inventory_dir, playbook.as_ref()]);
        for tag in &self.tags {
            cmd.args(["-t", &tag]);
        }
        for v in self.extra_vars.iter() {
            cmd.args(["-e", v]);
        }
        if self.verbosity > 0 {
            let mut v: String = "-".into();
            v.push_str(&"v".repeat(self.verbosity));
            cmd.arg(v);
        }
        if self.check {
            cmd.arg("--check");
        }
        if self.diff {
            cmd.arg("--diff");
        }
        let args: Vec<&str> = cmd.get_args().map(|s| s.to_str().unwrap()).collect();
        let args_str = args.join(" ");
        println!("ansible-playbook {args_str}");
        for v in self.hidden_extra_vars.iter() {
            cmd.args(["-e", v]);
        }
        let mut session = Session::spawn(cmd).unwrap();
        let opts = InteractOptions::default();
        let vault_pass = self.vault_pass.clone();
        let pass_prompts = self.pass_prompts.clone();
        let pass_prompts = pass_prompts.into_iter();
        let prompts = self.prompts.clone();
        let prompts = prompts.into_iter();
        let opts = opts.input_filter(|input| {
            if let [3] = input {
                if !prompt_opt("Cancel? [y]").unwrap().unwrap_or(false) {
                    return Ok(Cow::Borrowed(&[]));
                }
            }
            Ok(Cow::Borrowed(input))
        });
        let opts = pass_prompts.fold(opts, move |opts, prompt| {
            let vault_pass = vault_pass.clone();
            opts.on_output(prompt, {
                move |mut ctx: Context<Session, _, _, _>, _| {
                    let s = ctx.session();
                    s.send(&vault_pass.clone())?;
                    s.send("\n")?;
                    Ok(())
                }
            })
        });
        let mut opts = prompts.fold(opts, move |opts, (prompt, resp)| {
            opts.on_output(prompt, {
                move |mut ctx: Context<Session, _, _, _>, _| {
                    let s = ctx.session();
                    s.send(&resp.clone())?;
                    s.send("\n")?;
                    Ok(())
                }
            })
        });
        opts.interact_in_terminal(&mut session).unwrap();
        match session.wait().unwrap() {
            Exited(_, 0) => (),
            Exited(_, 99) => exit(1),
            Exited(_, code) => panic!("bad return code: {code}"),
            status => {
                unreachable!("{status:?}")
            }
        }
    }
}
